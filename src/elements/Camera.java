package elements;

import java.util.ArrayList;

import primitives.*;

public class Camera {
	final Point3D _p0;
	final Vector _vUp;
	final Vector _vTo;
	final Vector _vRight;
	/**
	 * Camera constructor 
	 * @param _p0
	 * @param _vUp
	 * @param _vTo
	 */
	public Camera(Point3D _p0, Vector _vTo, Vector _vUp) {
		super();
		this._p0 = _p0;
		this._vUp = _vUp.normalize();
		this._vTo = _vTo.normalize();
		if(_vUp.dotProduct(_vTo) != 0)
			throw new IllegalArgumentException("vector _vUp is not orthogonal to _vTo");
		_vRight = (_vTo.crossProduct(_vUp)).normalize();
	}
	/**
     * getter the center point of the camera
     * @return the center of the camera
     */
	public Point3D get_p0() {
		return _p0;
	}
	/**
     * getter the vector up of the camera
     * @return the _vUp of the camera
     */
	public Vector get_vUp() {
		return _vUp;
	}
	/**
     * getter the vector to of the camera
     * @return the _vTo of the camera
     */
	public Vector get_vTo() {
		return _vTo;
	}
	/**
     * getter the vector right of the camera
     * @return the _vRight of the camera
     */
	public Vector get_vRight() {
		return _vRight;
	}
	/**
	 * Get the view plane parameters and then construct the Ray Through Pixel i,j
	 * @param nX number of pixel of the view plane in axisX
	 * @param nY number of pixel of the view plane in axisY
	 * @param j the j index of pixel
	 * @param i the i index of pixel
	 * @param screenDistance the distance between the view plane and the camera
	 * @param screenWidth the width of the view plane
	 * @param screenHeight the height of the view plane
	 */
	public Ray constructRayThroughPixel (int nX, int nY, int j, int i, double screenDistance, double screenWidth, double screenHeight)
	{
		//Pc = P0 + d*Vto;
		Point3D pc = _p0.add(_vTo.scale(screenDistance));
		//Ry = h/Ny
		double rY = screenHeight/nY;
		//Rx = w/Nx
		double rX = screenWidth/nX;
		//yi = (i � Ny/2)*Ry + Ry/2
		double yi = (i - nY/2.0)*rY + rY/2;		
		//xj = (j � Nx/2)*Rx + Rx/2
		double xj = (j - nX/2.0)*rX + rX/2;
		//Pi,j = Pc + (xj*vright � yi*vup)
		Point3D pi_j = pc;
		if (xj != 0) pi_j = pi_j.add(_vRight.scale(xj));
		if (yi != 0) pi_j = (pi_j.subtract(_vUp.scale(yi).get_head())).get_head();
		//vi,j = Pi,j � P0
		Vector vi_j = pi_j.subtract(_p0);
		//Ray: {_p0 = P0, _direction = normalize(Vi,j) }
		Ray ri_j = new Ray(_p0, vi_j.normalize());
		return ri_j;
	}
	/**
	 * Get the view plane parameters and then construct 4 Rays Through Pixel i,j
	 * @param nX number of pixel of the view plane in axisX
	 * @param nY number of pixel of the view plane in axisY
	 * @param j the j index of pixel
	 * @param i the i index of pixel
	 * @param level of adaptive Super Sampling - we used 4
	 * @param place witch part and place in the pixel
	 * @param screenDistance the distance between the view plane and the camera
	 * @param screenWidth the width of the view plane
	 * @param screenHeight the height of the view plane
	 * @return a list of 4 rays in all the 4 corners of the pixel i,j
	 */
	public ArrayList<Ray> constructRaysThroughPixel (int nX, int nY, int j, int i, int level, int place, double screenDistance, double screenWidth, double screenHeight)
	{
		//for the 4 rays
		ArrayList<Ray> rays = new ArrayList<Ray>();
		//to get the initial place in the pixel in the view plane appropriate to 'place' and 'level'
		double [] xy = new double [2];
		xy = calcYiXj(nX, nY, j, i, level, place, screenWidth, screenHeight);
		//Pc = P0 + d*Vto;
		Point3D pc = _p0.add(_vTo.scale(screenDistance));
		//Ry = h/Ny
		double rY = screenHeight/nY;
		//Rx = w/Nx
		double rX = screenWidth/nX;
		double mechane = Math.pow(2, level - 1);
		
		//Create a ray in the lower left corner of the pixel
		//Pi,j = Pc + (xj*vright � yi*vup)
		Point3D pi_j = pc;	
		//move in the pixel to the appropriate place the we calculated in the function 'calcYiXj'
		if (xy[0] != 0) pi_j = pi_j.add(_vRight.scale(xy[0]));
		if (xy[1] != 0) pi_j = (pi_j.subtract(_vUp.scale(xy[1]).get_head())).get_head();
		//xy[1] = nY * place * rY;
		//vi,j = Pi,j � P0
		Vector vi_j = pi_j.subtract(_p0);
		//Ray: {_p0 = P0, _direction = normalize(Vi,j) }
		Ray ri_j = new Ray(_p0, vi_j.normalize());
		rays.add(ri_j);
		
		//Create a ray in the upper left corner of the pixel
		//yi = (i � Ny/2)*Ry + Ry 
		//dividing in mechane to get the appropriate place of this ray
		xy[1] = (i - nY/2.0)*rY + rY / mechane;		
		//xj = (j � Nx/2)*Rx 
		xy[0] = (j - nX/2.0)*rX;
		//Pi,j = Pc + (xj*vright � yi*vup)
		pi_j = pc;
		//move in the pixel to the appropriate place
		if (xy[0] != 0) pi_j = pi_j.add(_vRight.scale(xy[0]));
		if (xy[1] != 0) pi_j = (pi_j.subtract(_vUp.scale(xy[1]).get_head())).get_head();
		//vi,j = Pi,j � P0
		vi_j = pi_j.subtract(_p0);
		//Ray: {_p0 = P0, _direction = normalize(Vi,j) }
		ri_j = new Ray(_p0, vi_j.normalize());
		rays.add(ri_j);
		
		//Create a ray in the upper right corner of the pixel
		//yi = (i � Ny/2)*Ry + Ry / mechane
		//dividing in mechane to get the appropriate place of this ray
		xy[1] = (i - nY/2.0)*rY + rY / mechane;		
		//xj = (j � Nx/2)*Rx + Rx / mechane
		xy[0] = (j - nX/2.0)*rX + rX / mechane;
		//Pi,j = Pc + (xj*vright � yi*vup)
		pi_j = pc;
		//move in the pixel to the appropriate place
		if (xy[0] != 0) pi_j = pi_j.add(_vRight.scale(xy[0]));
		if (xy[1] != 0) pi_j = (pi_j.subtract(_vUp.scale(xy[1]).get_head())).get_head();
		//vi,j = Pi,j � P0
		vi_j = pi_j.subtract(_p0);
		//Ray: {_p0 = P0, _direction = normalize(Vi,j) }
		ri_j = new Ray(_p0, vi_j.normalize());
		rays.add(ri_j);
		
		//Create a ray in the lower right corner of the pixel
		//dividing in mechane to get the appropriate place of this ray	
		//yi = (i � Ny/2)*Ry
		xy[1] = (i - nY/2.0)*rY ;		
		//xj = (j � Nx/2)*Rx + Rx / mechane
		xy[0] = (j - nX/2.0)*rX + rX / mechane;
		//Pi,j = Pc + (xj*vright � yi*vup)
		pi_j = pc;
		//move in the pixel to the appropriate place
		if (xy[0] != 0) pi_j = pi_j.add(_vRight.scale(xy[0]));
		if (xy[1] != 0) pi_j = (pi_j.subtract(_vUp.scale(xy[1]).get_head())).get_head();
		//vi,j = Pi,j � P0
		vi_j = pi_j.subtract(_p0);
		//Ray: {_p0 = P0, _direction = normalize(Vi,j) }
		ri_j = new Ray(_p0, vi_j.normalize());
		rays.add(ri_j);
		
		return rays;//the 4 rays in the corners
	}
	/**
	 * 
	 * @param n a number to convert to binary number
	 * @param numOfBits - the number of bits in the binary number
	 * @return a string of n in binary number with numOfBits bits
	 */
	public static String intToBinary (int n, int numOfBits) {
		   String binary = "";
		   for(int i = 0; i < numOfBits; ++i, n/=2) {
		      switch (n % 2) {
		         case 0:
		            binary = "0" + binary;
		         break;
		         case 1:
		            binary = "1" + binary;
		         break;
		      }
		   }

		   return binary;
		}
	/**
	 * 
	  @param nX number of pixel of the view plane in axisX
	 * @param nY number of pixel of the view plane in axisY
	 * @param j the j index of pixel
	 * @param i the i index of pixel
	 * @param level of adaptive Super Sampling - we used 4
	 * @param place witch part and place in the pixel
	 * @param screenWidth the width of the view plane	 
	 * @param screenHeight the height of the view plane
	 * @return array of 2 values that show us the appropriate place in the j, i pixel
	 */
	public double[] calcYiXj(int nX, int nY, int j, int i, int level, int place, double screenWidth, double screenHeight)
	{
		//array of 2 values that show us the appropriate place in the j, i pixel
		double [] xy = new double [2];
		//Ry = h/Ny
		double rY = screenHeight/nY;
		//Rx = w/Nx
		double rX = screenWidth/nX;
		//the place in binary number (we need max 6 bits appropriate to the level - 4)
		String binaryPlace = intToBinary(place, 6);
		int yPlace1 = 0, xPlace1 = 0, yPlace2 = 0, xPlace2 = 0;
		int xPlace = binaryPlace.indexOf(7 - level);//how much to move in X to get to the outside down quarter
		int yPlace = binaryPlace.indexOf(6 - level);//how much to move in Y to get to the outside down quarter
		
		//how much to move appropriate to the level
		double mechane = Math.pow(2, 1);
		//yi = (i � Ny/2)*Ry 
		xy[1] = (i - nY/2.0)*rY + rY * (yPlace / mechane);		
		//xj = (j � Nx/2)*Rx 
		xy[0] = (j - nX/2.0)*rX + rX * (xPlace / mechane);
		
		if (level == 3)//how much move to the inner quadrant
		{
			yPlace1 = binaryPlace.indexOf(4);
			xPlace1 = binaryPlace.indexOf(5);
		}
		else//level 4 //how much move to the more inner quadrant
		{
			
			yPlace1 = binaryPlace.indexOf(2);
			xPlace1 = binaryPlace.indexOf(3);
			yPlace2 = binaryPlace.indexOf(4);
			xPlace2 = binaryPlace.indexOf(5);
		}
		
		
		if(level == 3 || level == 4)//how much move to the more inner quadrant
		{
			mechane = Math.pow(2, 2);
			//yi = (i � Ny/2)*Ry 
			xy[1] = (i - nY/2.0)*rY + rY * (yPlace1 / mechane);		
			//xj = (j � Nx/2)*Rx 
			xy[0] = (j - nX/2.0)*rX + rX * (xPlace1 / mechane);
		}
		if(level == 4)//how much move to the more inner quadrant
		{
			mechane = Math.pow(2, 3);
			//yi = (i � Ny/2)*Ry 
			xy[1] = (i - nY/2.0)*rY + rY * (yPlace2 / mechane);		
			//xj = (j � Nx/2)*Rx 
			xy[0] = (j - nX/2.0)*rX + rX * (xPlace2 / mechane);
		}
		//return 2 values that show us the appropriate place in the j, i pixel
		return xy;
	}

}
